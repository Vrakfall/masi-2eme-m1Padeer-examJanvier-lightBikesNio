import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jérémy Lecocq.
 * Imported from a previous projet: BattleArenaRmi from Jérémy Lecocq for August exams.
 *
 * @author Jérémy Lecocq
 */
public class CrudHashMapSingleValues<K, V> extends HashMap<K, V> implements Serializable, Map<K, V>, Cloneable {
  //==== Static variables ====//

  //==== Attributes ====//

  //==== Getters and Setters ====//

  //==== Constructors ====//


  public CrudHashMapSingleValues(int initialCapacity, float loadFactor) {
    super(initialCapacity, loadFactor);
  }

  public CrudHashMapSingleValues(int initialCapacity) {
    super(initialCapacity);
  }

  public CrudHashMapSingleValues() {
    super();
  }

  public CrudHashMapSingleValues(Map<? extends K, ? extends V> m) {
    super(m);
  }


  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  /**
   * @param key
   * @param value
   * @return True if the add was successful, false otherwise.
   */
  @Override
  public synchronized V put(K key, V value) {
    if (key == null
        || value == null
        || (key instanceof String && ((String) key).isEmpty())
        || (value instanceof String && ((String) value).isEmpty())
        || containsKey(key)
        || containsValue(value)
        ) {
      return null;
    }


    return super.put(key, value);
  }

  public CrudHashMapSingleValues<K, V> getShallowCopy() {
    return new CrudHashMapSingleValues<>(this);
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
