import java.nio.channels.SocketChannel;

/**
 * Created by James Greenfield.
 *
 * @author James Greenfield
 */
public class ChangeRequest {
  //==== Static variables ====//
  public static final int REGISTER = 1;
  public static final int CHANGEOPS = 2;

  //==== Attributes ====//
  public SocketChannel socket;
  public int type;
  public int ops;

  //==== Getters and Setters ====//

  //==== Constructors ====//
  public ChangeRequest(SocketChannel socket, int type, int ops) {
    this.socket = socket;
    this.type = type;
    this.ops = ops;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
