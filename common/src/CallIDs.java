/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public interface CallIDs {
  byte CONNECT = 0;
  byte RETURN_CONNECT = CONNECT + 1;
  byte CREATE_GAME = RETURN_CONNECT + 1;
  byte RETURN_CREATE_GAME = CREATE_GAME + 1;
  byte JOIN_GAME = RETURN_CREATE_GAME + 1;
  byte RETURN_JOIN_GAME = JOIN_GAME + 1;
  byte START_GAME = RETURN_JOIN_GAME + 1;
  byte RETURN_START_GAME = START_GAME + 1;
  byte UPDATE = RETURN_START_GAME + 1;
  byte GET_WAITING_GAMES = UPDATE + 1;
  byte RETURN_WAITING_GAMES = GET_WAITING_GAMES + 1;
  byte DIRECTION = RETURN_WAITING_GAMES + 1;
}
