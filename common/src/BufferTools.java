import java.io.EOFException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public class BufferTools {
  //==== Static variables ====//

  //====== Static Methods ======//
  //======== Public ========//

  public static byte[] readXBytes(final SocketChannel socketChannel, final int bytesNumberToRead) throws IOException {
    return readChannel(socketChannel, bytesNumberToRead).array();
  }

  public static byte readByte(final SocketChannel socketChannel) throws IOException {
    final ByteBuffer byteBuffer = ByteBuffer.allocate(1);
    final int readBytesAmount = socketChannel.read(byteBuffer);

    if (readBytesAmount < 0) {
      throw new EOFException("Channel contains EOF.");
    }
    if (readBytesAmount != 1) {
      throw new IOException("Didn't read the amount of bytes excpected. Expected 1, read " + readBytesAmount);
    }

    byteBuffer.flip();
    return byteBuffer.get();
  }

  public static short readShort(final SocketChannel socketChannel) throws IOException {
    return readChannel(socketChannel, 2).getShort();
  }

  public static String readString(final SocketChannel socketChannel, final int bytesNumberToRead) throws IOException {
    ByteBuffer byteBuffer = readChannel(socketChannel, bytesNumberToRead);
    return new String(byteBuffer.array(), 0, byteBuffer.limit());
  }

  public static Object readObject(final SocketChannel socketChannel, final int bytesNumberToRead) throws IOException {
    byte[] readBytes = readXBytes(socketChannel, bytesNumberToRead);
    try {
      return IOTools.unserialize(readBytes);
    } catch (IOException e) {
      System.err.println("Cannot unserialize this object!!! Must be checked!");
      e.printStackTrace();
      System.err.println("Cannot unserialize this object!!! Must be checked!");
    } catch (ClassNotFoundException e) {
      System.err.println("Cannot unserialize this object because the class was not found!!! Must be checked!");
      e.printStackTrace();
      System.err.println("Cannot unserialize this object because the class was not found!!! Must be checked!");
    }
    return null; //TODO Make an exception for this
  }

  public static byte[] shortToBytes(final short number) {
    final ByteBuffer byteBuffer = ByteBuffer.allocate(2);
    byteBuffer.putShort(number);
    return byteBuffer.array();
  }

  public static byte[] intToBytes(final int number) {
    final ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    byteBuffer.putInt(number);
    return byteBuffer.array();
  }

  public static byte[] numberToBytes(final long number, final int bytesNumber) {
    final ByteBuffer byteBuffer = ByteBuffer.allocate(bytesNumber);
//    byteBuffer.putLong();
    return null;
  }

  public static byte[] objectToBytes(final Serializable serializable) {
    try {
      return IOTools.serialize(serializable);
    } catch (IOException e) {
      System.err.println("Cannot serialize this object!!! Must be checked!");
      e.printStackTrace();
      System.err.println("Cannot serialize this object!!! Must be checked!");
    }
    return null;
  }

  public static byte[] posIntTo3UnsignedBytes(final int number) {
    System.out.println(">>>> "+number);
    if (0 <= number && number <= 16777215) {
      final byte[] tempBytes = intToBytes(number);
      final ByteBuffer byteBuffer = ByteBuffer.allocate(3);
      byteBuffer.put(tempBytes, 1, 3);
      System.out.println("<<< +" + Arrays.toString(byteBuffer.array()));
      return byteBuffer.array();
    }
    return null; //TODO Throw an exception instead
  }

  public static int pos3UnsignedBytesToInt(byte[] bytes) throws IOException {
  System.out.println(">>>> "+ Arrays.toString(bytes));
    final ByteBuffer byteBufferIn = ByteBuffer.wrap(bytes);
    final ByteBuffer byteBufferOut = ByteBuffer.allocate(4);
    byteBufferOut.put((byte) 0);
    byte[] tempBytes = byteBufferIn.array();
    if (tempBytes.length != 3) {
      throw new IOException("Not the right amount of bytes to read. Expected 3, got " + tempBytes.length);
    }
    byteBufferOut.put(tempBytes);
    byteBufferOut.flip();
    int tempInt =byteBufferOut.getInt();
    System.out.println("<<< "+tempInt);
    return tempInt;
  }

  public static long maxStringSizeForXBytes(final int bytesNumber) {
    return maxIntForUnsignedXBytes(bytesNumber); //-1 just in case of an EOL
  }

  public static long maxIntForUnsignedXBytes(final int bytesNumber) {
    return Math.round(Math.pow(2, bytesNumber*8)-1);
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//
  private static ByteBuffer readChannel(SocketChannel socketChannel, final int bytesNumberToRead) throws IOException {
    final ByteBuffer byteBuffer = ByteBuffer.allocate(bytesNumberToRead);
    final int readBytesAmount = socketChannel.read(byteBuffer);

    if (readBytesAmount < 0) {
      throw new EOFException("Channel contains EOF.");
    }
    if (readBytesAmount != bytesNumberToRead) {
      throw new IOException("Didn't read the amount of bytes excpected. Expected " + bytesNumberToRead + ", read " + new String(byteBuffer.array(), 0, byteBuffer.limit()));
    }

    byteBuffer.flip();
    return byteBuffer;
  }
}

