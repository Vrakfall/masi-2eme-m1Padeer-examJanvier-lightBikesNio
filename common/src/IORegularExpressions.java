/**
 * Created by Jérémy Lecocq.
 * Imported from previous projects: HTTPS Shop from Jérémy Lecocq & Luther Claude Dianga (projet SSL Pâques)
 * and Chat RMI August exams (Jérémy Lecocq).
 *
 * @author Jérémy Lecocq
 */
interface IORegularExpressions {
  String IP_ADDRESS = "(?!^(?:0\\.0\\.0\\.0|255\\.255\\.255\\.255)$)(^(?:(?:1?[\\d]?[\\d]|2(?:[0-4][\\d]|5[0-4]))[.]){3}(?:1?[\\d]?[\\d]|2(?:[0-5][\\d]|5[0-5]))$)";
  int IP_ADDRESS_GROUP = 1;

  String HOSTNAME = "(?=^.{4,253}$)^((?:(?!-)[a-zA-Z0-9-]{1,63}(?<!-)\\.)+[a-zA-Z]{2,63}|localhost)$";
  int HOSTNAME_GROUP = 1;

  String USERNAME = "^(\\S{1," + CommonParameters.USERNAME_MAX_LENGTH + "})$";
  int USERNAME_GROUP = 1;
}
