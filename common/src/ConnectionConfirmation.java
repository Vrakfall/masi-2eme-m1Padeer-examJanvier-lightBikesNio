import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class ConnectionConfirmation implements Serializable {
  //==== Static variables ====//

  //==== Attributes ====//
  private UUID uuid;
  private GameListElement[] gameList;
  private boolean isNoGameWaiting;

  //==== Getters and Setters ====//

  public UUID getUuid() {
    return uuid;
  }

  public GameListElement[] getGameList() {
    return gameList;
  }

  public boolean isNoGameWaiting() {
    return isNoGameWaiting;
  }

  //==== Constructors ====//

  public ConnectionConfirmation(UUID uuid, GameListElement[] gameList) {
    this.uuid = uuid;
    this.gameList = gameList;
    this.isNoGameWaiting = false;
  }

  public ConnectionConfirmation(UUID uuid) {
    this.uuid = uuid;
    this.isNoGameWaiting = true;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
