import java.awt.*;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public interface CommonParameters {
  //==== Limitations ====//
  int USERNAME_MAX_LENGTH = 16;

  //==== Networking ====//
  int MAXIMUM_TRIES_NUMBER = 3;

  //==== Protocol ====//
  long TIME_OUT = 1000;
  int HEADER_PROTOCOL_VERSION_SIZE = 2;
  short HEADER_PROTOCOL_VERSION = 0;
  int HEADER_CALL_ID_SIZE_V_1 = 1;
  int HEADER_PAYLOAD_INDICATOR_SIZE_V_1 = 3;
  long MAX_SIZE_FOR_PAYLOAD = BufferTools.maxStringSizeForXBytes(HEADER_PAYLOAD_INDICATOR_SIZE_V_1);

  //==== Grid ====//
  int GRID_MINIMUM_X = 0;
  int GRID_MAXIMUM_X = 99;
  int GRID_SIZE_X = GRID_MAXIMUM_X - GRID_MINIMUM_X +1;

  int GRID_MINIMUM_Y = 0;
  int GRID_MAXIMUM_Y = 99;
  int GRID_SIZE_Y = GRID_MAXIMUM_Y - GRID_MINIMUM_Y +1;

  //==== Grid's Cells ====//

  int GRID_CELL_WIDTH = 4;
  int GRID_CELL_HEIGHT = 4;

  //==== AIs ====//
  //Every player starts in the middle of a border segment
  int PLAYER_DEFAULT_STARTING_X_POSITION = 50;
  int PLAYER_DEFAULT_STARTING_Y_POSITION = 99;
  char PLAYER_DEFAULT_STARTING_CAR_DIRECTION = 'U';
  String PLAYER_DEFAULT_NAME = "Unnamed player";

  String AI_01_DEFAULT_NAME = "AI01";
  Color AI_01_DEFAULT_COLOR = Color.BLUE;
  int AI_01_DEFAULT_STARTING_X_POSITION = 0;
  int AI_01_DEFAULT_STARTING_Y_POSITION = 50;
  char AI_01_DEFAULT_STARTING_CAR_DIRECTION = 'R';

  String AI_02_DEFAULT_NAME = "AI02";
  Color AI_02_DEFAULT_COLOR = Color.YELLOW;
  int AI_02_DEFAULT_STARTING_X_POSITION = 50;
  int AI_02_DEFAULT_STARTING_Y_POSITION = 0;
  char AI_02_DEFAULT_STARTING_CAR_DIRECTION = 'D';

  String AI_03_DEFAULT_NAME = "AI03";
  Color AI_03_DEFAULT_COLOR = Color.GREEN;
  int AI_03_DEFAULT_STARTING_X_POSITION = 99;
  int AI_03_DEFAULT_STARTING_Y_POSITION = 50;
  char AI_03_DEFAULT_STARTING_CAR_DIRECTION = 'L';

  Color POSITION_01_DEFAULT_COLOR = Color.RED;
  Color POSITION_02_DEFAULT_COLOR = Color.BLUE;
  Color POSITION_03_DEFAULT_COLOR = Color.YELLOW;
  Color POSITION_04_DEFAULT_COLOR = Color.GREEN;
}
