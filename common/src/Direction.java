import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public class Direction implements Serializable {
  //==== Static variables ====//

  //==== Attributes ====//
  private UUID uuid;
  private char direction;

  //==== Getters and Setters ====//

  public UUID getUuid() {
    return uuid;
  }

  public char getDirection() {
    return direction;
  }

  //==== Constructors ====//

  public Direction(UUID uuid, char direction) {
    this.uuid = uuid;
    this.direction = direction;
  }


  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
