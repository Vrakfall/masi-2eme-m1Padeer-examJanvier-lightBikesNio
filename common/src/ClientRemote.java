//import java.rmi.Remote;
//import java.rmi.RemoteException;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
interface ClientRemote {
  String ERROR_WINDOW_TITLE = "An error occurred!";

  String GAME_NAME_EXISTS_ERROR_MESSAGE = "This game name already exists, please choose a new one.";

//  void startGame(GameState gameState) throws RemoteException;

//  void update(GameState gameState);
}
