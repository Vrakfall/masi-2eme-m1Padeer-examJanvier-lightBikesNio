import java.awt.*;
import java.io.Serializable;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class Player implements CommonParameters, Serializable {
  //==== Static variables ====//

  //==== Attributes ====//

  private String nickname;
  private Color color;
  private int score;

  // Game Properties

  /**
   * Position of the players on the x axis (columns; 0 = left)
   */
  private int positionX;

  /**
   * Position of the players on the y axis (lines  ; 0 = top)
   */
  private int positionY;

  /**
   * Are the players human or not?
   */
  private boolean isHuman;

  /**
   * Current player orientation; 'U'p  'D'own 'L'eft 'R'ight
   */
  private char carDirection;

  /**
   * Are players still in play or are they eliminated?
   */
  private boolean isAlive;

  private boolean isWinner;

  //==== Getters and Setters ====//

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @return
   */
  public String getNickname() {
    return nickname;
  }

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @param nickname
   */
  public void setNickname(String nickname) {
    if (nickname != null && !nickname.isEmpty()) {
      this.nickname = nickname;
    }
  }

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @return
   */
  public Color getColor() {
    return color;
  }

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @param color
   */
  public void setColor(Color color) {
    if (color != null) {
      this.color = color;
    }
  }

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @return
   */
  public int getScore() {
    return score;
  }

  /**
   * Parts are imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @param score
   */
  private void setScore(int score) {
    if (score >= 0) {
      this.score = score;
    }
  }

  public int getPositionX() {
    return positionX;
  }

  public void setPositionX(int xPosition) {
    if (xPosition >= 0) {
      this.positionX = xPosition;
    }
  }

  public int getPositionY() {
    return positionY;
  }

  public void setPositionY(int yPosition) {
    if (yPosition >= 0) {
      this.positionY = yPosition;
    }
  }

  public boolean isHuman() {
    return isHuman;
  }

  public void setHuman(boolean human) {
    isHuman = human;
  }

  public char getCarDirection() {
    return carDirection;
  }

  public void setCarDirection(char carDirection) {
    if (carDirection == 'U' || carDirection == 'R' || carDirection == 'D' || carDirection == 'L') {
      this.carDirection = carDirection;
    }
  }

  public boolean isAlive() {
    return isAlive;
  }

  public void setAlive(boolean alive) {
    isAlive = alive;
  }

  public boolean isWinner() {
    return isWinner;
  }

  public void setWinner(boolean winner) {
    isWinner = winner;
  }

  //==== Constructors ====//


  public Player(String nickname, Color color, int score, int positionX, int positionY, boolean isHuman, char carDirection, boolean isAlive) {
    setNickname(nickname);
    setColor(color);
    setScore(score);
    setPositionX(positionX);
    setPositionY(positionY);
    setHuman(isHuman);
    setCarDirection(carDirection);
    setAlive(isAlive);
    isWinner = false;
  }

  public Player(String nickname, Color color, int positionX, int positionY, boolean isHuman, char carDirection) {
    setNickname(nickname);
    setColor(color);
    setScore(0);
    setPositionX(positionX);
    setPositionY(positionY);
    setHuman(isHuman);
    setCarDirection(carDirection);
    setAlive(false);
    isWinner = false;
  }

  public Player(Player player) {
    setNickname(player.getNickname());
    setColor(player.getColor());
    setScore(player.getScore());
    setPositionX(player.getPositionX());
    setPositionY(player.getPositionY());
    setHuman(player.isHuman());
    setCarDirection(player.getCarDirection());
    setAlive(player.isAlive());
    isWinner = player.isWinner;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @param o
   * @return
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Player player = (Player) o;

    if (!getNickname().equals(player.getNickname())) return false;
    return getColor().equals(player.getColor());

  }

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @return
   */
  @Override
  public int hashCode() {
    int result = this.getClass().getName().hashCode();
    result = 31 * result + getNickname().hashCode();
    return result;
  }

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  public void incrementScore() {
    if (isAlive()) {
      ++score;
    }
  }

  public void updatePostion(Color[][] gameGrid) {
    //If the player is still in play
    if (isAlive()) {
      //Update its position based on the previous position and the current direction
      //If we hit the wall or the path of any player, it's game over
      switch (getCarDirection()) {
        case 'L':
          if (getPositionX() > GRID_MINIMUM_X && gameGrid[getPositionX() - 1][getPositionY()].equals(Color.BLACK)) {
            setPositionX(getPositionX() - 1);
          } else {
            setAlive(false);
          }
          break;
        case 'R':
          if (getPositionX() < 99 && gameGrid[getPositionX() + 1][getPositionY()].equals(Color.black)) {
            setPositionX(getPositionX() + 1);
          } else {
            setAlive(false);
          }
          break;
        case 'U':
          if (getPositionY() > 0 && gameGrid[getPositionX()][getPositionY() - 1].equals(Color.black)) {
            setPositionY(getPositionY() - 1);
          } else {
            setAlive(false);
          }
          break;
        case 'D':
          if (getPositionY() < 99 && gameGrid[getPositionX()][getPositionY() + 1].equals(Color.black)) {
            setPositionY(getPositionY() + 1);
          } else {
            setAlive(false);
          }
          break;
      }
    }
  }

  //======== Protected ========//

  //======== Private-Package ========//

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   *
   * @param i
   */
  void addToScore(int i) {
    if (i > 0) {
      score += i;
    }
  }

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
