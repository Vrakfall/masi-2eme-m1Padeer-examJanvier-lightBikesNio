import java.awt.*;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class GameState implements CommonParameters, Serializable {
  //==== Static variables ====//

  //==== Attributes ====//

  private CrudHashMapSingleValues<String, Player> players;
  Color[][] grid;
  Integer[][] timers;

  //==== Getters and Setters ====//

//  public CrudHashMapSingleValues<String, Player> getPlayers() {
//    return players;
//  }

  public Player getPlayer(String name) {
    return players.get(name);
  }

  //==== Constructors ====//

  public GameState(CrudHashMapSingleValues<String, Player> players) {
    this.players = players;

    initializeBoard();
  }

  public GameState(CrudHashMapSingleValues<String, Player> players, Color[][] grid, Integer[][] timers) {
    this.players = players;
    this.grid = grid;
    this.timers = timers;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  public int getNumberPlayersAlive() {
    int numberPlayersAlive = 0;
    for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
      if (playerEntry.getValue().isAlive()) {
        numberPlayersAlive++;
      }
    }
    return numberPlayersAlive;
  }

  public boolean isGameOver() {
    return getNumberPlayersAlive() <= 1;
  }

  public Player getWinner() throws NoWinnerException {
    int playersNumber = getNumberPlayersAlive();
    if (playersNumber <= 1) {
      for (Map.Entry<String, Player> playerEntry : players.entrySet()) {
        if (playerEntry.getValue().isWinner()) {
          return playerEntry.getValue();
        }
      }
    }

    throw new NoWinnerException(playersNumber);
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  private void initializeBoard() {
    grid = new Color[GRID_SIZE_X][GRID_SIZE_Y];

    for (int x=GRID_MINIMUM_X; x < GRID_MAXIMUM_X; ++x) {
      for (int y = GRID_MINIMUM_Y; y < GRID_MAXIMUM_Y; ++y) {
        grid[x][y] = Color.BLACK;
        timers[x][y] = 0;
      }
    }
  }

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
