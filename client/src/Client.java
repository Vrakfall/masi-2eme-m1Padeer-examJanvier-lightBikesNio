import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
//import java.rmi.RemoteException;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Sam, Jérémy Lecocq
 */
public class Client implements CommonParameters, ClientRemote {
  //==== Static variables ====//

  //==== Attributes ====//

  //  private Registry registry;
//  private ServerRemote serverRemote;
  private Player thisPlayer;
  private String nickname = "UnnamedPlayer";

  private GameRemote gameRemote;
  private boolean isGameStarted;

  private GUI gui;
  private GameState gameState;

  private UUID uuid;
  private GameListElement[] waitingGames;
  private SocketSelectorClient socketSelectorClient;
  private Thread socketSelectorClientThread;

  //====== UI ======//

  private Login loginForm;
  private Room roomForm;
  private JFrame currentWindow;
  private JFrame popupWindow;

  //==== Getters and Setters ====//

//  /**
//   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
//   *
//   * @param registry
//   */
//  void setRegistry(Registry registry) {
//    if (registry != null) {
//      this.registry = registry;
//    }
//  }

//  /**
//   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
//   *
//   * @param serverRemote
//   */
//  void setServerRemote(ServerRemote serverRemote) {
//    if (serverRemote != null) {
//      this.serverRemote = serverRemote;
//    }
//  }

  public Player getThisPlayer() {
    return thisPlayer;
  }


  //==== Constructors ====//

  /**
   * Imported from a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
   */
  public Client() {
    isGameStarted = false;
    this.gui = new GUI(this);
    this.thisPlayer = new Player(PLAYER_DEFAULT_NAME, Color.RED, CommonParameters.PLAYER_DEFAULT_STARTING_X_POSITION, CommonParameters.PLAYER_DEFAULT_STARTING_Y_POSITION, true, CommonParameters.PLAYER_DEFAULT_STARTING_CAR_DIRECTION);

    // UI stuff
    this.loginForm = new Login(this);
    this.roomForm = new Room(this);
    switchToFrame(loginForm.getJFrame());

    socketSelectorClientThread = new Thread(socketSelectorClient);

//    try {
//      UnicastRemoteObject.exportObject(this, 0);
//    } catch (RemoteException e) {
//      e.printStackTrace();
//    }
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  public static void main(String[] args) {
    Client client = new Client();

//    try {
//      client.connectAndCreateGame();
//    } catch (InvocationTargetException | InterruptedException | RemoteException e) {
//      e.printStackTrace(); //TODO Handle this exception
//    }

    //This is the GUI object, responsible for all the displays
//    final GUI gui = new GUI(client);

    //Lauching the GUI, using Swing EDT to remain "thread-safe".
//    try {
//      java.awt.EventQueue.invokeAndWait(new Runnable() {
//        public void run() {
//          gui.setVisible(true);
//        }
//      });
//    } catch (InterruptedException | InvocationTargetException e) {
//      e.printStackTrace();
//    }

    //This is the "real" GUI thread, in the sense that it's the one that will periodically trigger the graphical updates, still using the EDT.
//    while (client.) {
//      try {
//        Thread.sleep(50);
//        java.awt.EventQueue.invokeAndWait(new Runnable() {
//          public void run() {
//            gGUI.update();
//          }
//        });
//
//      } catch (Exception e) {
//        //TODO Better error handling
//        e.printStackTrace();
//      }
//
//    }

//    connectionWindow = ConnectionWindow.getInstance();
//    connectionWindow.initialize(client);
//    connectionWindow.setVisible(true);

//    waitingForPlayersWindow = WaitingForPlayersWindow.getInstance();
//    waitingForPlayersWindow.initialize(client);

//    client.connectAndCreateWaitingGame(CommonParameters.SEVER_IP);
//    client.createWaitingGame();
//    client.startGame();
  }

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  public void keyPressed(KeyEvent keyEvent) {
    // A key has been pressed. If a game is in progress, we must warn the core
    if (isGameStarted) {
      switch (keyEvent.getKeyCode()) {
        case KeyEvent.VK_LEFT:
          socketSelectorClient.sendDirection(new Direction(uuid, 'L'));
          break;
        case KeyEvent.VK_RIGHT:
          socketSelectorClient.sendDirection(new Direction(uuid, 'R'));
          break;
        case KeyEvent.VK_UP:
          socketSelectorClient.sendDirection(new Direction(uuid, 'U'));
          break;
        case KeyEvent.VK_DOWN:
          socketSelectorClient.sendDirection(new Direction(uuid, 'D'));
          break;
        default:
          break;
      }
    }
  }

//  public void connectAndCreateGame() throws InvocationTargetException, InterruptedException, RemoteException {
//    connectToServer("127.0.0.1");
//    gameRemote = serverRemote.connectAndCreateWaitingGame(new NewGameRequest(thisPlayer, "gameTest", 4));
//    EventQueue.invokeAndWait(() -> gui.setVisible(true));
//  }

  public void connect(String serverAddress, String nickname) {
    thisPlayer.setNickname(nickname);
    System.out.println("Connection attempt to " + serverAddress + ":" + ServerRemote.SERVER_PORT + ".");

    socketSelectorClient = new SocketSelectorClient(serverAddress, ServerRemote.SERVER_PORT, this);
    socketSelectorClientThread = new Thread(socketSelectorClient);
    socketSelectorClientThread.start();
//    ConnectionConfirmation connectionConfirmation = serverRemote.getListOfGames(nickname, this);
//    this.uuid = connectionConfirmation.getUuid();
//    if (!connectionConfirmation.isNoGameWaiting()) {
//      roomForm.setWaitingGames(connectionConfirmation.getGameList());
//    }
    System.out.println("Initiated connection with nickname: " + nickname);
  }

  public void receiveConnect(UUID uuid) {
    this.uuid = uuid;
    System.out.println("Successfully connected with uuid: " + uuid);
    switchToFrame(roomForm.getJFrame());
  }

  public void createGame(String gameName) {
//    gameRemote = serverRemote.createWaitingGame(new NewGameRequest(thisPlayer, gameName, uuid));
    socketSelectorClient.createGame(new NewGameRequest(thisPlayer, gameName, uuid));
    //TODO Handle when the client wasn't connected on the server or the name of the game was already taken, which shouldn't happen
  }

  public void receiveCreateGame(boolean gameAdded) {
    if (gameAdded) {
      System.out.println("Successfully created the game!");
      switchToFrame(gui);
    } else {
      roomForm.warn(GAME_NAME_EXISTS_ERROR_MESSAGE, ERROR_WINDOW_TITLE, JOptionPane.ERROR_MESSAGE);
    }
  }

  public void getWaitingGames() {
//    return serverRemote.getListOfGames(uuid);
    socketSelectorClient.getWaitingGames(uuid);
  }

  public void receiveWaitingGames(GameListElement[] gamesList) {
    roomForm.receiveWaitingGames(gamesList);
  }

  public void joinGame(String gameName) {
//    gameRemote = serverRemote.joinGame(new JoinGameRequest(thisPlayer, gameName, uuid));
    socketSelectorClient.joinGame(new JoinGameRequest(thisPlayer, gameName, uuid));
  }

  public void receiveJoinGame(boolean joined) {
    switchToFrame(gui);
  }

  public void startGame() {
    socketSelectorClient.startGame(uuid);
  }

  public void receiveStartGame(GameState gameState) {
    isGameStarted = true;
    this.gameState = gameState;
  }

  public void update(GameState gameState) {
    this.gameState = gameState;
    gui.update(gameState, thisPlayer);

    if (gameState.isGameOver()) {
      try {
        EventQueue.invokeAndWait(() -> {
          try {
            gui.showGameOver(gameState.getWinner());
          } catch (NoWinnerException e) {
            //This should never happen because we first checked that the game over but we catch it, just in case.
            e.printStackTrace();
            System.err.println(e.getMessage());
          }
        });
      } catch (InterruptedException | InvocationTargetException e) {
        e.printStackTrace();
        //This shouldn't happen either
      }
      switchToFrame(roomForm.getJFrame());
    }
  }

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  /**
   * Méthode permet de passer d'une fenêtre à une autre facilement.
   * Classe importée depuis le projet "Combat RPG" fait pour le cours de mise à niveau Java (MA) en Janvier 2016 par Jérémy Lecocq
   *
   * @param frame La fenêtre à afficher.
   */
  public void switchToFrame(JFrame frame) {
    disposeTopWindow();
    if (currentWindow != null)
      currentWindow.dispose();
    currentWindow = frame;
    currentWindow.setVisible(true);
  }

  /**
   * Méthode qui permet d'afficher facilement une fenêtre en pop-up.
   * Classe importée depuis le projet "Combat RPG" fait pour le cours de mise à niveau Java (MA) en Janvier 2016 par Jérémy Lecocq
   *
   * @param frame La fenêtre pop-up à afficher.
   */
  public void showPopupWindow(JFrame frame) {
    if (popupWindow != null)
      popupWindow.dispose();
    popupWindow = frame;
    popupWindow.setVisible(true);
  }

  /**
   * Méthode qui permet de facilement fermer le pop-up en cours.
   * Classe importée depuis le projet "Combat RPG" fait pour le cours de mise à niveau Java (MA) en Janvier 2016 par Jérémy Lecocq
   */
  public void disposeTopWindow() {
    if (popupWindow != null) {
      popupWindow.dispose();
      popupWindow = null;
    }
  }

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//
}
