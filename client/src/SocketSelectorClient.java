import java.io.EOFException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public class SocketSelectorClient implements Runnable, CommonParameters, CallIDs {
  private static final int clients = 10;

  private String hostname;
  private InetSocketAddress address;
  private SocketChannel socketChannel = null;
  private final Set<ChangeRequest> changeRequests;
  private final Set<ByteBuffer[]> pendingData;

  private Selector selector;
  final private Client client;

  public SocketSelectorClient(String hostname, int port, Client client) {
    this.hostname = hostname;
    this.address = new InetSocketAddress(hostname, port); //TODO Replace it so it is interactive
    changeRequests = Collections.synchronizedSet(new HashSet<>());
    pendingData = Collections.synchronizedSet(new HashSet<>());
    this.client = client;
  }

  private void initialize() throws IOException {
    socketChannel = SocketChannel.open(address);
    socketChannel.configureBlocking(false);
    selector = Selector.open();
    socketChannel.register(selector, SelectionKey.OP_READ);
  }

  private void readChannel(SelectionKey key) throws IOException {
//    final ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

    final SocketChannel keySocketChannel = (SocketChannel) key.channel();

    try {
      if (HEADER_PROTOCOL_VERSION_SIZE == 2) { //Useless check but still, it could help avoid a "gotcha" later
        final short inProtocolVersion = BufferTools.readShort(keySocketChannel);
        System.out.println("Received protocol version: " + inProtocolVersion);

        if (inProtocolVersion == HEADER_PROTOCOL_VERSION &&
            HEADER_CALL_ID_SIZE_V_1 == 1 &&
            HEADER_PAYLOAD_INDICATOR_SIZE_V_1 == 3) {//Useless checks but still, it could help avoid a "gotcha" later
          final byte inCallId = BufferTools.readXBytes(keySocketChannel, HEADER_CALL_ID_SIZE_V_1)[0];
          System.out.println("Received Call Id: " + inCallId);
//          final short inPayloadIndicator = BufferTools.readShort(keySocketChannel);
          int inPayloadIndicator;
          try {
            inPayloadIndicator = BufferTools.pos3UnsignedBytesToInt(BufferTools.readXBytes(keySocketChannel, 3));
            System.out.println("Received payload size: " + inPayloadIndicator);
          } catch (IOException e) {
            e.printStackTrace();
            return;
          }


          if (inPayloadIndicator > 0) {
//            String payload = BufferTools.readString(keySocketChannel, inPayloadIndicator);

//            System.out.println("Success. Header: P: " + inProtocolVersion + " - CId: " + inCallId + " - PI: " + inPayloadIndicator + " - Payload in String: " + payload);

//            String inString;
            Object inO = null;
            boolean inBoolean = false;

            switch (inCallId) {
              case RETURN_CONNECT:
              case RETURN_START_GAME:
              case UPDATE:
              case RETURN_WAITING_GAMES:
                inO = BufferTools.readObject(socketChannel, inPayloadIndicator);
                break;
              case RETURN_CREATE_GAME:
              case RETURN_JOIN_GAME:
                inBoolean = BufferTools.readByte(socketChannel) == 1;
                break;
            }

            switch (inCallId) {
              case RETURN_CONNECT:
                if (inO instanceof UUID) {
                  client.receiveConnect((UUID)inO);
                }
                break;
              case RETURN_CREATE_GAME:
                client.receiveCreateGame(inBoolean);
                break;
              case RETURN_JOIN_GAME:
                client.receiveJoinGame(inBoolean);
                break;
              case RETURN_START_GAME:
                if (inO instanceof GameState) {
                  client.receiveStartGame((GameState) inO);
                }
                break;
              case UPDATE:
                if (inO instanceof GameState) {
                  client.update((GameState) inO);
                }

              case RETURN_WAITING_GAMES:
                if (inO instanceof GameListElement[]) {
                  client.receiveWaitingGames((GameListElement[]) inO);
                }
                break;
            }
          }
        }
      }
      //TODO Else throw an exception because of a wrong protocol version

    } catch (EOFException e) {
      keySocketChannel.close();
    }
  }

  private synchronized void checkForChangeRequests() {
    synchronized (changeRequests) {
      changeRequests.forEach((final ChangeRequest changeRequest) -> {
        if (changeRequest.type == ChangeRequest.CHANGEOPS) {
          changeRequest.socket.keyFor(selector).interestOps(changeRequest.ops);
        }
      });

      changeRequests.clear();
    }
  }

  @Override
  public void run() {
    try {
      initialize();
      connect(client.getThisPlayer().getNickname());

      while (true) {
        checkForChangeRequests();
        if (selector.select(TIME_OUT) > 0) {
          synchronized (selector.selectedKeys()) {
            selector.selectedKeys().forEach((final SelectionKey key) -> {
              try {
                if (key.isReadable()) {
                  readChannel(key);

                } else if (key.isWritable()) {
                  System.out.println("It is writable! \\o/");
                  writeChannel(key);

                } else {
  //              SocketChannel keySocketChannel = (SocketChannel) key.channel();
  //              try {
  //                keySocketChannel.close();
  //              } catch (IOException e) {
  //                e.printStackTrace();
  //              }
                  System.err.println("An interest in an operation might have be forgotten.");
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
            });
          }
        }
        selector.selectedKeys().clear();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private synchronized void writeChannel(SelectionKey key) {
    SocketChannel keySocketChannel = (SocketChannel) key.channel();

    synchronized (pendingData) {
      pendingData.forEach((final ByteBuffer[] byteBuffers) -> {
        try {
            keySocketChannel.write(byteBuffers);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
      pendingData.clear();
    }
    key.interestOps(SelectionKey.OP_READ);
  }

  public void send(byte callID, byte[] data) {
    if (data.length > MAX_SIZE_FOR_PAYLOAD) {
      return;
    }

    final byte[][] wholeMessage = new byte[4][];
    wholeMessage[0] = new byte[HEADER_PROTOCOL_VERSION_SIZE];
    wholeMessage[1] = new byte[HEADER_CALL_ID_SIZE_V_1];
    wholeMessage[2] = new byte[HEADER_PAYLOAD_INDICATOR_SIZE_V_1];
    wholeMessage[3] = new byte[data.length];

    ByteBuffer[] byteBuffers = new ByteBuffer[4];
    for (int i = 0; i < 4; i++) {
      byteBuffers[i] = ByteBuffer.wrap(wholeMessage[i]);
    }
    byteBuffers[0].putShort(HEADER_PROTOCOL_VERSION)
        .flip();
    byteBuffers[1].put(callID)
        .flip();
//    byteBuffers[2].putShort((short) data.length) //Shouldn't check for the number as it's checked above
//        .flip();

    wholeMessage[2] = BufferTools.posIntTo3UnsignedBytes(data.length);
    if (wholeMessage[2] == null) {
      return;
    }
    byteBuffers[2] = ByteBuffer.wrap(wholeMessage[2]);

    byteBuffers[3].put(data)
        .flip();

//    System.out.println("from " + data.length + " to ");
//    for (int i = 0; i < wholeMessage[2].length; i++) {
//      System.out.println(wholeMessage[2][i] + "");
//    }

    if (wholeMessage[0].length > HEADER_PAYLOAD_INDICATOR_SIZE_V_1 || wholeMessage[1].length > HEADER_CALL_ID_SIZE_V_1) {
      return;
      //TODO Throw an exception
    }


    changeRequests.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

    pendingData.add(byteBuffers);

    selector.wakeup();
  }

  private void send(byte callID) {
    final byte[][] wholeMessage = new byte[3][];
    wholeMessage[0] = new byte[HEADER_PROTOCOL_VERSION_SIZE];
    wholeMessage[1] = new byte[HEADER_CALL_ID_SIZE_V_1];
    wholeMessage[2] = new byte[HEADER_PAYLOAD_INDICATOR_SIZE_V_1];

    ByteBuffer[] byteBuffers = new ByteBuffer[3];
    for (int i = 0; i < 4; i++) {
      byteBuffers[i] = ByteBuffer.wrap(wholeMessage[i]);
    }

    byteBuffers[0].putShort(HEADER_PROTOCOL_VERSION)
        .flip();
    byteBuffers[1].put(callID)
        .flip();

    wholeMessage[2] = BufferTools.posIntTo3UnsignedBytes(0);
    if (wholeMessage[2] == null) {
      return;
    }
    byteBuffers[2] = ByteBuffer.wrap(wholeMessage[2]);


    if (wholeMessage[0].length > HEADER_PAYLOAD_INDICATOR_SIZE_V_1 || wholeMessage[1].length > HEADER_CALL_ID_SIZE_V_1) {
      return;
      //TODO Throw an exception
    }

    changeRequests.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

    pendingData.add(byteBuffers);

    selector.wakeup();
  }

//  public void oldRun() {
//    for (int i = 0; i < 10; i++) {
//      if (message.length() > MAX_SIZE_FOR_PAYLOAD) {
//        continue;
//      }
//
//      byteBuffer.clear();
//      byteBuffer.put(BufferTools.shortToBytes(CommonParameters.HEADER_PROTOCOL_VERSION));
//      byteBuffer.put((byte) 0);
//      byteBuffer.put(BufferTools.shortToBytes((short) message.getBytes().length));
//      System.out.println(Arrays.toString(BufferTools.shortToBytes((short) message.getBytes().length)));
//      byteBuffer.put(message.getBytes());
//      byteBuffer.flip();
//      try {
//        socketChannel.write(byteBuffer);
//        sleep(500);
//      } catch (IOException | InterruptedException e) {
//        e.printStackTrace();
//      }
//    }
//  }

//  public static void main(String[] args) {
//    int port = 9001;
//    for (int i = 0; i < clients; i++) {
//      new Thread(new SocketSelectorClient("Client " + i + "'s poke.", port)).start();
//      System.out.println("Client " + i + "'s poke.");
//    }
//  }

  public void connect(String nickname) {
    send(CONNECT, nickname.getBytes());
  }

  public void createGame(NewGameRequest newGameRequest) {
    send(CREATE_GAME, BufferTools.objectToBytes(newGameRequest));
  }

  public void startGame(UUID uuid) {
    send(START_GAME, BufferTools.objectToBytes(uuid));
  }

  public void joinGame(JoinGameRequest joinGameRequest) {
    send(JOIN_GAME, BufferTools.objectToBytes(joinGameRequest));
  }

  public void getWaitingGames(UUID uuid) {
    send(GET_WAITING_GAMES, BufferTools.objectToBytes(uuid));
  }

  public void sendDirection(Direction direction) {
    send(DIRECTION, BufferTools.objectToBytes(direction));
  }
}
