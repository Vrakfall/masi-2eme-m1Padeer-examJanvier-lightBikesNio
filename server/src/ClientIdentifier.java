import java.nio.channels.SocketChannel;
import java.util.UUID;

/**
 * Created by Jérémy Lecocq.
 *
 * @author Jérémy Lecocq
 */
public class ClientIdentifier {
  //==== Static variables ====//

  //==== Attributes ====//
  private final UUID uuid;
  private final String nickname;
  private final SocketChannel socketChannel;
  private Game currentGame;

  //==== Getters and Setters ====//

  public UUID getUuid() {
    return uuid;
  }

  public String getNickname() {
    return nickname;
  }

  public SocketChannel getSocketChannel() {
    return socketChannel;
  }

  public Game getCurrentGame() {
    return currentGame;
  }

  public void setCurrentGame(Game currentGame) {
    this.currentGame = currentGame;
  }

  //==== Constructors ====//

  public ClientIdentifier(String nickname, SocketChannel socketChannel) {
    this.uuid = UUID.randomUUID();
    this.nickname = nickname;
    this.socketChannel = socketChannel;
  }

  //==== Lists' CRUDs ====//

  //==== Usual Methods ====//

  //==== Custom Methods ====//
  //====== Object Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

  //====== Static Methods ======//
  //======== Public ========//

  //======== Protected ========//

  //======== Private-Package ========//

  //======== Private ========//

}
