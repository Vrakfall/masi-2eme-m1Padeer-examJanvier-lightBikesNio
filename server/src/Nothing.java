/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public class Nothing {
  public static void main(String[] args) {
    System.out.println((Short.MAX_VALUE + Math.abs(Short.MIN_VALUE) + 1)/2);
    System.out.println(Math.pow(2,16)/2-2);
    short test = (short) (Short.MAX_VALUE);
    System.out.println(test);

    String string = "this is a string";
    System.out.println(string.length());
    System.out.println(string.getBytes().length);
  }
}
