import java.awt.*;
import java.util.ArrayList;

/**
 * This is a very simple AI. Basically, it tries to avoid the edges, and the previous paths, except some fresh ones (because it's supposed to be easy to beat it)
 * Most of the time, it stays on track, otherwise it plays at random given the previous constraints.
 *
 * @author Sam
 */
public class EasyAI extends AI {

  public EasyAI(String nickname, Color color, int xPosition, int yPosition, boolean isHuman, char carDirection) {
    super(nickname, color, xPosition, yPosition, isHuman, carDirection);
  }

  /**
   * Method implemented over the interface.
   *
   * @param iGrid  a copy of the current grid representation
   * @param iTimer a copy of the current freshness information
   * @return a character as a movement decision ('U', 'R', 'L', 'D').
   */
  public void setNewDir(Color[][] iGrid, Integer[][] iTimer) {
    //The returned character
    char rc;

    //These will contain the possible moves, given the constraints
    ArrayList<Character> vPossibleMoves = new ArrayList<>();

    //The four options so far
    char[] cPossibleMoves = new char[4];
    cPossibleMoves[0] = 'U';
    cPossibleMoves[1] = 'R';
    cPossibleMoves[2] = 'D';
    cPossibleMoves[3] = 'L';

    //change the character representation of the direction into an integer one
    int directionInt = getCarDirectionInt();

    //For each possible new direction
    for (int currentCarDirectionInt = 0; currentCarDirectionInt < 4; currentCarDirectionInt++) {
      //Should this direction be added? For the moment, no
      boolean to_add = false;

      //We don't accept half-turns as an option, since it's not in the game rule
      if (directionInt != (currentCarDirectionInt + 2) % 4) {
        //If this move leads to go over the border, we forbid it
        if ((getPositionY() > 0 && currentCarDirectionInt == 0) || (getPositionX() < 99 && currentCarDirectionInt == 1) || (getPositionY() < 99 && currentCarDirectionInt == 2) || (getPositionX() > 0 && currentCarDirectionInt == 3)) {
          //Get the next tile to reach after the move, and its freshness
          Color targetTileColor = getTargetTileElement(iGrid, currentCarDirectionInt);
          int targetTimer = getTargetTileElement(iTimer, currentCarDirectionInt);

          //If this tile is empty, we consider this as a valid move
          if (targetTileColor.equals(Color.BLACK))
            to_add = true;
          else {
            //This is where we trick the AI to be worse than it could be
            //It has a random chance to consider a fresh path as a free tile
            //The fresher the path, the better the odds. (between 0% and 10%)
            double rchoice = Math.random();
            if (rchoice < (double) (targetTimer / 10)) {
              //Don't see that it's already taken (and die)
              to_add = true;
            }
          }
        }
      }

      //Add this direction into the array of possible moves if it respects the constraints
      if (to_add) {
        vPossibleMoves.add(cPossibleMoves[currentCarDirectionInt]);
      }
    }

    char temporaryDirection = getCarDirection();

    //If there's something to choose from
    if (vPossibleMoves.size() > 0) {

      //Is the current direction still an option?
      boolean found = vPossibleMoves.contains(getCarDirection());
      if (found) {
        //More likely to stay on course. Only has a 5% probability to change course
        if (Math.random() < 0.05) {
          //Choose one new direction at random
          int iSize = vPossibleMoves.size();
          int iRandom = (int) (Math.random() * iSize);
          temporaryDirection = vPossibleMoves.get(iRandom);
        }

      } else {
        //The current direction is invalid (leads to a path or over the border)
        //Select a new one at random.
        int iSize = vPossibleMoves.size();
        int iRandom = (int) (Math.random() * iSize);
        temporaryDirection = vPossibleMoves.get(iRandom);
      }
    }

    //Update the direction (or keep the existing one)
    setCarDirection(temporaryDirection);
  }
}
