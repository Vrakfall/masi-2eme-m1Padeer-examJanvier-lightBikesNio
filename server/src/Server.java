import java.io.Serializable;
import java.nio.channels.SocketChannel;
//import java.rmi.AlreadyBoundException;
//import java.rmi.RemoteException;
import java.util.*;

/**
 * Created by Jérémy Lecocq.
 * Parts of this class are inspired by a previous project: BattleArenaRmi from Jérémy Lecocq for 2016's August exams.
 *
 * @author Jérémy Lecocq
 */
public class Server implements Serializable, ServerRemote {
  //==== Static variables ====//

  final private SocketSelectorServer socketSelectorServer;
  final private Map<String, Game> games;
  final private Map<String, Game> waitingGames;
  final private Map<String, Game> playingGames;
  final private Map<UUID, ClientIdentifier> connectedClients;

  //==== Attributes ====//

  //==== Getters and Setters ====//

  public SocketSelectorServer getSocketSelectorServer() {
    return socketSelectorServer;
  }

  //==== Constructors ====//
  public Server() {
    super();
    games = Collections.synchronizedMap(new HashMap<String, Game>());
    waitingGames = Collections.synchronizedMap(new HashMap<>());
    playingGames = Collections.synchronizedMap(new HashMap<>());
    connectedClients = Collections.synchronizedMap(new HashMap<>());

    socketSelectorServer = new SocketSelectorServer(this);

    System.out.println("Server is started");
  }

  //==== Methods ====//

  private boolean addGame(Game game) {
    if (games.containsKey(game.getName()) || waitingGames.containsKey(game.getName())) {
      return false;
    }
    games.put(game.getName(), game);
    waitingGames.put(game.getName(), game);
    return true;
  }

//  @Override
//  public GameRemote connectAndCreateWaitingGame(NewGameRequest newGameRequest) throws RemoteException {
//    Game game = new Game(newGameRequest);
//    addGame(game);
//    return game;
//  }

  public Game getGameByClientUUID(UUID uuid) {
    return connectedClients.get(uuid).getCurrentGame();
  }

  public SocketChannel getSocketByClientUUID(UUID uuid) {
    return connectedClients.get(uuid).getSocketChannel();
  }

  public ClientIdentifier getClientIdentifierByUUID(UUID uuid) {
    return connectedClients.get(uuid);
  }

  public void receiveDirection(Direction direction) {
    if (!uuidIsConnected(direction.getUuid())) {
      return;
    }

    ClientIdentifier clientIdentifier = getClientIdentifierByUUID(direction.getUuid());

    if (clientIdentifier == null) {
      return;
    }

    clientIdentifier.getCurrentGame().receiveNewDirection(clientIdentifier.getNickname(), direction.getDirection());
  }

  public void createWaitingGame(NewGameRequest newGameRequest, SocketChannel socketChannel) {
    UUID uuid = newGameRequest.getUuid();
    if (!uuidIsConnected(uuid)) {
      socketSelectorServer.returnCreateGame(socketChannel, false);
      return;
    }

    ClientIdentifier client = connectedClients.get(uuid);
    if (!client.getSocketChannel().equals(socketChannel)) {
      socketSelectorServer.returnCreateGame(socketChannel, false);
      return;
    }

    Game game = new Game(newGameRequest, this);
    if (addGame(game)) {

      client.setCurrentGame(game);
      System.out.println("Successfully created the game!");
      socketSelectorServer.returnCreateGame(client.getSocketChannel(), true);
      return;
    }

    socketSelectorServer.returnCreateGame(client.getSocketChannel(), false);
  }

  public void joinGame(JoinGameRequest joinGameRequest, SocketChannel socketChannel) {
    UUID uuid = joinGameRequest.getUuid();
    if (!uuidIsConnected(uuid)) {
      socketSelectorServer.returnJoinGame(socketChannel, false);
      return;
    }

    ClientIdentifier client = connectedClients.get(uuid);
    if (!client.getSocketChannel().equals(socketChannel)) {
      socketSelectorServer.returnJoinGame(socketChannel, false);
      return;
    }

    Game game = games.get(joinGameRequest.getGameName());

    if (game == null) {
      socketSelectorServer.returnJoinGame(socketChannel, false);
      return;
    }

    game.addPlayerWithClient(new InnerPlayerWithClient(joinGameRequest.getPlayer(), joinGameRequest.getUuid()));
    socketSelectorServer.returnJoinGame(socketChannel, true);
  }

  public void connect(String nickname, SocketChannel socketChannel) {
    System.out.println("Received connection attempt from " + nickname);
    ClientIdentifier clientIdentifier = new ClientIdentifier(nickname, socketChannel);
    if (connectedClients.put(clientIdentifier.getUuid(), clientIdentifier) != null) {
      // this shouldn't happen. However, if it does, let's just recall the method and generate a new and random uuid.
      connect(nickname, socketChannel);
      //TODO Implement a check so it doesn't do too many recursive calls.........
    }
    socketSelectorServer.returnConnect(socketChannel, clientIdentifier.getUuid());
  }

  public void getWaitingGames(UUID uuid, SocketChannel socketChannel) {
    if (!uuidIsConnected(uuid) || waitingGames.size() <= 0) {
      socketSelectorServer.returnWaitingGames(socketChannel, null);
      return;
    }

    ClientIdentifier client = connectedClients.get(uuid);
    if (!client.getSocketChannel().equals(socketChannel)) {
      socketSelectorServer.returnJoinGame(socketChannel, false);
      return;
    }

    ArrayList<GameListElement> gameList = new ArrayList<>(waitingGames.size()); //The size can change but we preset it for performance optimization.

    synchronized (waitingGames) {
      for (Map.Entry<String, Game> gameEntry : waitingGames.entrySet()) {
        Game game = gameEntry.getValue();
        gameList.add(new GameListElement(game.getName(), game.getNumberOfPlayers()));
      }
    }

    if (gameList.isEmpty()) {
      socketSelectorServer.returnJoinGame(socketChannel, false);
      return;
    }

    socketSelectorServer.returnWaitingGames(socketChannel, gameList.toArray(new GameListElement[gameList.size()]));
  }

  private boolean uuidIsConnected(UUID uuid) {
    synchronized (connectedClients) {
      for (Map.Entry<UUID, ClientIdentifier> clientIdentifierEntry : connectedClients.entrySet()) {
        if (clientIdentifierEntry.getValue().getUuid().equals(uuid)) {
          return true;
        }
      }
    }
    return false;
  }


  public void switchGameToStarted(String gameName) {
    playingGames.put(gameName, waitingGames.get(gameName));
    waitingGames.remove(gameName);
  }

  public void removeGame(String gameName) {
    System.out.println("Game dropped " + gameName);
    games.remove(gameName);
    playingGames.remove(gameName);
    waitingGames.remove(gameName);
  }

  public void removePlayer(UUID uuid) {
    System.out.println("Dropped client: " + connectedClients.get(uuid));
    connectedClients.remove(uuid);
  }

  public static void main(String[] args) {
//    System.setProperty("java.rmi.server.hostname", "192.168.0.1");
    Server server = new Server();
    new Thread(server.socketSelectorServer).start();
  }
}
