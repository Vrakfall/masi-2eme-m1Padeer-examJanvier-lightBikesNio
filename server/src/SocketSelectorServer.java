import java.io.EOFException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.*;

/**
 * Created by Jérémy "Vrakfall" Lecocq.
 *
 * @author Jérémy "Vrakfall" Lecocq
 */
public class SocketSelectorServer implements Runnable, CommonParameters, CallIDs, ServerRemote {
  private static final int byteBufferSize = 1024;

  private final Set<SocketChannel> clients;
  private Selector selector;
  private ServerSocketChannel serverSocketChannel;
  private int oldClientAmount = 0;
  private final Set<ChangeRequest> changeRequests;
  private final Map<SocketChannel, Set<ByteBuffer[]>> pendingData;

  private Server server;

  public SocketSelectorServer(Server server) {
    clients = Collections.synchronizedSet(new HashSet<>());
    selector = null;
    oldClientAmount = clients.size();
    changeRequests = Collections.synchronizedSet(new HashSet<>());
    pendingData = Collections.synchronizedMap(new HashMap<>());
    this.server = server;
  }

//  public static void main(String[] args) {
//    SocketSelectorServer socketSelectorServer = new SocketSelectorServer();
//
//    (new Thread(socketSelectorServer)).start();
//  }

  private void checkForChangeRequests() {
    synchronized (changeRequests) {
      changeRequests.forEach((final ChangeRequest changeRequest) -> {
        if (changeRequest.type == ChangeRequest.CHANGEOPS) {
          changeRequest.socket.keyFor(selector).interestOps(changeRequest.ops);
        }
      });
      changeRequests.clear();
    }
  }

  @Override
  public void run() {
    try {
      initialize();

      System.out.println("Connected clients: " + clients.size());
      while (true) {
        int newClientAmount = clients.size();
        if (oldClientAmount != newClientAmount) {
          System.out.println("Connected clients: " + clients.size());
          oldClientAmount = newClientAmount;
        }

        checkForChangeRequests();

        if (selector.select(TIME_OUT) > 0) {
          synchronized (selector.selectedKeys()) {
            selector.selectedKeys().forEach((final SelectionKey key) -> {
              try {
                if (key.isAcceptable()) {

                  acceptConnection(key);

                } else if (key.isReadable()) {
                  readChannel(key);

                } else if (key.isWritable()) {
                  writeChannel(key);

                } else {
//                SocketChannel keySocketChannel = (SocketChannel) key.channel();
//                clients.remove(keySocketChannel);
//                keySocketChannel.close();

                  System.err.println("An interest in an operation might have be forgotten.");
                }
              } catch (IOException e) {
                e.printStackTrace();
              }
            });
          }
        }

        selector.selectedKeys().clear();
//        System.out.println("Cleared");
      }
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      synchronized (clients) {
        clients.forEach((socketChannel) -> {
          try {
            closeChannel(socketChannel);
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
      }

      try {
        selector.close();
        serverSocketChannel.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

  }

  private void initialize() throws IOException {
    serverSocketChannel = ServerSocketChannel.open();
    selector = Selector.open();
    SelectorProvider.provider().openSelector();

    serverSocketChannel.socket().bind(new InetSocketAddress(SERVER_PORT));
    serverSocketChannel
        .configureBlocking(false)
        .register(selector, SelectionKey.OP_ACCEPT);

    System.out.println("Server listening on port " + serverSocketChannel.socket().getLocalPort());
  }

  private void acceptConnection(SelectionKey key) throws IOException {
    final ServerSocketChannel keyServerSocketChannel = (ServerSocketChannel) key.channel();

    try {
      final SocketChannel newClientSocketChannel = keyServerSocketChannel.accept();

      newClientSocketChannel
          .configureBlocking(false)
          .register(selector, SelectionKey.OP_READ);
      clients.add(newClientSocketChannel);
    } catch (IOException e) {
      keyServerSocketChannel.close();
    }
  }

  private void readChannel(SelectionKey key) throws IOException {
//    final ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

    final SocketChannel keySocketChannel = (SocketChannel) key.channel();

    try {
      if (HEADER_PROTOCOL_VERSION_SIZE == 2) { //Useless check but still, it could help avoid a "gotcha" later
        final short inProtocolVersion = BufferTools.readShort(keySocketChannel);
        System.out.println("Received protocol version: " + inProtocolVersion);

        if (inProtocolVersion == HEADER_PROTOCOL_VERSION &&
            HEADER_CALL_ID_SIZE_V_1 == 1 &&
            HEADER_PAYLOAD_INDICATOR_SIZE_V_1 == 3) {//Useless checks but still, it could help avoid a "gotcha" later (and it did), because the following is only correct THAT number of bytes.
          final byte inCallId = BufferTools.readByte(keySocketChannel);
          System.out.println("Received Call Id: " + inCallId);
//          final short inPayloadIndicator = BufferTools.readShort(keySocketChannel);
//          final int inPayloadIndicator = BufferTools.pos3UnsignedBytesToInt(BufferTools.readXBytes(keySocketChannel, 3));
//          System.out.println("Received payload size: " + inPayloadIndicator);

          int inPayloadIndicator;
          try {
            inPayloadIndicator = BufferTools.pos3UnsignedBytesToInt(BufferTools.readXBytes(keySocketChannel, 3));
            System.out.println("Received payload size: " + inPayloadIndicator);
          } catch (IOException e) {
            e.printStackTrace();
            return;
          }

          if (inPayloadIndicator > 0) {
//            String payload = BufferTools.readString(keySocketChannel, inPayloadIndicator);
//
//            System.out.println("Success. Header: P: " + inProtocolVersion + " - CId: " + inCallId + " - PI: " + inPayloadIndicator + " - Payload in String: " + payload);

            String inString = "";
            Object inO = null;

            switch (inCallId) {
              case CONNECT:
                inString = BufferTools.readString(keySocketChannel, inPayloadIndicator);
                break;
              case CREATE_GAME:
              case START_GAME:
              case JOIN_GAME:
              case GET_WAITING_GAMES:
              case DIRECTION:
                inO = BufferTools.readObject(keySocketChannel, inPayloadIndicator);
                break;
            }

            //TODO Replace these with threads?
            switch (inCallId) {
              case CONNECT:
                if (!inString.isEmpty()) {
                  server.connect(inString, keySocketChannel);
                }
                break;

              case CREATE_GAME:
                if (inO instanceof NewGameRequest) {
                  server.createWaitingGame((NewGameRequest) inO, keySocketChannel);
                }
                break;

              case JOIN_GAME:
                if (inO instanceof JoinGameRequest) {
                  server.joinGame((JoinGameRequest) inO, keySocketChannel);
                }
                break;

              case START_GAME:
                if (inO instanceof UUID) {
                  server.getGameByClientUUID((UUID) inO).startGame();
                }
                break;

              case GET_WAITING_GAMES:
                if (inO instanceof UUID) {
                  server.getWaitingGames((UUID) inO, keySocketChannel);
                }
                break;

              case DIRECTION:
                if (inO instanceof Direction) {
                  server.receiveDirection((Direction) inO);
                }
                break;
            }
          }
        }
      }
      //TODO Else throw an exception because of a wrong protocol version

    } catch (EOFException e) {
      closeChannel(keySocketChannel);
    }
  }

  private synchronized void writeChannel(SelectionKey key) {
    SocketChannel keySocketChannel = (SocketChannel) key.channel();


    final Set<ByteBuffer[]> queue = pendingData.get(keySocketChannel);
    synchronized (queue) {
      queue.forEach((final ByteBuffer[] byteBuffers) -> {
        try {
            keySocketChannel.write(byteBuffers);
        } catch (IOException e) {
          e.printStackTrace();
        }
      });
      queue.clear();
    }
    key.interestOps(SelectionKey.OP_READ);
  }

  public void send(SocketChannel socketChannel, byte callID, byte[] data) {
    if (data.length > MAX_SIZE_FOR_PAYLOAD) {
      return;
    }

    final byte[][] wholeMessage = new byte[4][];
    wholeMessage[0] = new byte[HEADER_PROTOCOL_VERSION_SIZE];
    wholeMessage[1] = new byte[HEADER_CALL_ID_SIZE_V_1];
    wholeMessage[2] = new byte[HEADER_PAYLOAD_INDICATOR_SIZE_V_1];
    wholeMessage[3] = new byte[data.length];

    ByteBuffer[] byteBuffers = new ByteBuffer[4];
    for (int i = 0; i < 4; i++) {
      byteBuffers[i] = ByteBuffer.wrap(wholeMessage[i]);
    }

    byteBuffers[0].putShort(HEADER_PROTOCOL_VERSION)
        .flip();
    byteBuffers[1].put(callID)
        .flip();
//    byteBuffers[2].putShort((short) data.length) //Shouldn't check for the number as it's checked above
//        .flip();

    wholeMessage[2] = BufferTools.posIntTo3UnsignedBytes(data.length);
    if (wholeMessage[2] == null) {
      return;
    }
    byteBuffers[2] = ByteBuffer.wrap(wholeMessage[2]);

    byteBuffers[3].put(data)
        .flip();

    System.out.println("from " + data.length + " to ");
    for (int i = 0; i < wholeMessage[2].length; i++) {
      System.out.println(wholeMessage[2][i] + "");
    }

    if (wholeMessage[0].length > HEADER_PAYLOAD_INDICATOR_SIZE_V_1 || wholeMessage[1].length > HEADER_CALL_ID_SIZE_V_1) {
      return;
      //TODO Throw an exception
    }

    changeRequests.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

    Set<ByteBuffer[]> queue = pendingData.computeIfAbsent(socketChannel, k -> Collections.synchronizedSet(new HashSet<>())); //This is equal to the following:
    /*
      Set<ByteBuffer> queue = pendingData.get(socketChannel);
      if (queue == null) {
        queue = Collections.synchronizedSet(new HashSet<>());
        pendingData.put(socketChannel, queue);
      }
    */

    queue.add(byteBuffers);

    selector.wakeup();
  }

  private void send(SocketChannel socketChannel, byte callID) {
        final byte[][] wholeMessage = new byte[3][];
    wholeMessage[0] = new byte[HEADER_PROTOCOL_VERSION_SIZE];
    wholeMessage[1] = new byte[HEADER_CALL_ID_SIZE_V_1];
    wholeMessage[2] = new byte[HEADER_PAYLOAD_INDICATOR_SIZE_V_1];

    ByteBuffer[] byteBuffers = new ByteBuffer[3];
    for (int i = 0; i < 4; i++) {
      byteBuffers[i] = ByteBuffer.wrap(wholeMessage[i]);
    }

    byteBuffers[0].putShort(HEADER_PROTOCOL_VERSION)
        .flip();
    byteBuffers[1].put(callID)
        .flip();
    byteBuffers[2].put((byte) 0)
        .flip();

    if (wholeMessage[0].length > HEADER_PAYLOAD_INDICATOR_SIZE_V_1 || wholeMessage[1].length > HEADER_CALL_ID_SIZE_V_1) {
      return;
      //TODO Throw an exception
    }

    changeRequests.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

    Set<ByteBuffer[]> queue = pendingData.computeIfAbsent(socketChannel, k -> Collections.synchronizedSet(new HashSet<>())); //This is equal to the following:
    /*
      Set<ByteBuffer> queue = pendingData.get(socketChannel);
      if (queue == null) {
        queue = Collections.synchronizedSet(new HashSet<>());
        pendingData.put(socketChannel, queue);
      }
    */

    queue.add(byteBuffers);

    selector.wakeup();
  }

  private void closeChannel(SocketChannel socketChannel) throws IOException {
    clients.remove(socketChannel);
    socketChannel.close();
  }

  public void returnConnect(SocketChannel socketChannel, UUID uuid) {
    System.out.println("Returning this uuid: " + uuid);
    send(socketChannel, RETURN_CONNECT, BufferTools.objectToBytes(uuid));
  }

  public void returnCreateGame(SocketChannel socketChannel, boolean gameCreated) {
    byte[] bytes = {(byte)(gameCreated ? 1 : 0)};
    send(socketChannel, RETURN_CREATE_GAME, bytes);
  }

  public void returnJoinGame(SocketChannel socketChannel, boolean joined) {
    byte[] bytes = {(byte) (joined ? 1 : 0)};
    send(socketChannel, RETURN_JOIN_GAME, bytes);
  }

  public void returnStartGame(SocketChannel socketChannel, GameState gameState) {
    byte[] bytes = BufferTools.objectToBytes(gameState);
    System.out.println("Size of gameSate is " + bytes.length);
    send(socketChannel, RETURN_START_GAME, bytes);
  }

  public void update(SocketChannel socketChannel, GameState gameState) {
    byte[] bytes = BufferTools.objectToBytes(gameState);
    System.out.println("Size of gameSate is " + bytes.length);
    send(socketChannel, UPDATE, bytes);
  }

  public void returnWaitingGames(SocketChannel socketChannel, GameListElement[] gamesList) {
    send(socketChannel, RETURN_WAITING_GAMES, BufferTools.objectToBytes(gamesList));
  }
}
